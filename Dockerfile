FROM openjdk:8-jre-alpine
MAINTAINER TangQiang <1071180223@qq.com>

ARG JAR_FILE
ADD ${JAR_FILE} /app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app.jar"]